async function init() {
    const musics_list = await (await fetch("/proxy.php?list=1")).json();
    
    console.log(musics_list);
    
    for(let index = 0; index < musics_list.length; index++) {
        const code = '<div class="song amplitude-song-container amplitude-play-pause"  data-amplitude-song-index="'+index+'">'+
                '<span class="song-position">'+index+'</span>' +
                '<img class="song-album-art" data-amplitude-song-info="cover_art_url" data-amplitude-song-index="'+index+'"/>' +
                '<div class="song-meta-data-container">' +
                  '<span class="song-name" data-amplitude-song-info="name" data-amplitude-song-index="'+index+'"></span>' +
                  '<span class="song-artist" data-amplitude-song-info="artist" data-amplitude-song-index="'+index+'"></span>' +
                '</div>' +
              '</div>' +
          '</div>';
        
        document.getElementById("musicsTarget").innerHTML += code;
    }

    
    Amplitude.init({
        
        songs: musics_list.map(entry => {
            return {
                name: entry.title,
                artist: entry.artist,
                url: "/proxy.php/?music=" + entry.id,
                "cover_art_url": "/img/music.png"
            }
        }),
        
        
        bindings: {
            37: 'prev',
            39: 'next',
            32: 'play_pause'
        },
        debug: true,
        visualization: 'michaelbromley_visualization',
        
        waveforms: {
            sample_rate: 50
        },
        
        visualizations: [
            {
                object: MichaelBromleyVisualization,
                params: {
                    
                }
            }
        ]
    });
}

(async () => {
    try {
        await init();
    }
    catch(e) {
        console.error(e);
        alert("Failed to initialize configuration !");
    }
})();


document.getElementsByClassName('visualization-toggle')[0].addEventListener('click', function(){
    if( this.classList.contains( 'visualization-off' ) ){
      this.classList.remove('visualization-off');
      this.classList.add('visualization-on');
      document.getElementById('large-now-playing-album-art').style.display = 'none';
      document.getElementById('large-visualization').style.display = 'block';
    }else{
      this.classList.remove('visualization-on');
      this.classList.add('visualization-off');
      document.getElementById('large-now-playing-album-art').style.display = 'block';
      document.getElementById('large-visualization').style.display = 'none';
    }
  });
  
  
  document.getElementsByClassName('arrow-up-icon')[0].addEventListener('click', function(){
    document.getElementById('visualizations-player-playlist').style.display = 'block';
  });
  
  document.getElementsByClassName('arrow-down-icon')[0].addEventListener('click', function(){
    document.getElementById('visualizations-player-playlist').style.display = 'none';
  });