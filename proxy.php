<?php
const CONF_PATH = __DIR__."/private/config.private.json";

if (!file_exists(CONF_PATH)) {
    http_response_code(500);
    exit("Please initialize configuration!");
}

$conf = json_decode(file_get_contents(CONF_PATH));


if (isset($_GET["list"])) {
    // Create a stream
    $opts = array(
        'http' =>array(
            'method'=>"GET",
            "header" => "token:".$conf->token."\r\n"
        )
    );
    
    $context = stream_context_create($opts);
    
    // Open the file using the HTTP headers set above
    $file = file_get_contents($conf->url."list", false, $context);

    header("content-type: application/json");
    echo $file;
    exit(0);
}



if (isset($_GET["music"])) {
    $id = (int)($_GET["music"]);

    // Create a stream
    $opts = array(
        'http' =>array(
            'method'=>"GET",
            "header" => "token:".$conf->token."\r\n"
        )
    );
    
    $context = stream_context_create($opts);
    
    // Open the file using the HTTP headers set above
    $file = file_get_contents($conf->url."download/".$id, false, $context);

    header("content-type: audio/mpeg");
    echo $file;
    exit(0);
}


http_response_code(404);
print_r("Not found!");